import java.util.Scanner;
public class ApplianceStore
{
	public static void main(String[]args)
	{
		Scanner scan = new Scanner(System.in);
		Microwave[] microwave = new Microwave[4];
		for (int i = 0; i < 4; i++)
		{
			int microwaveNum = i + 1;
			microwave[i] = new Microwave();
			System.out.println("Enter the size of the microwave" + microwaveNum);
			microwave[i].size = scan.nextInt();
			System.out.println("Enter the Wattage of the microwave" + microwaveNum);
			microwave[i].watts = scan.nextInt();
			System.out.println("Enter the brand of the microwave" + microwaveNum);
			microwave[i].brand = scan.next();
		}
		System.out.println("the size of the microwave 4 is " + microwave[3].size + ", the wattage of the microwave 4 is " + microwave[3].watts + ", the brand of the microwave 4 is " + microwave[3].brand);
		System.out.println("Enter time in seconds");
		int seconds = scan.nextInt();
		microwave[0].printDetails();
		microwave[0].reheat(seconds);
	}
}