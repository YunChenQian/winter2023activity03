
public class Microwave
{
	public int watts;
	public String brand;
	public int size;
	
	public void printDetails()
	{
		System.out.println("This microwave has " + this.watts + " watts and the size is " + this.size + " and the microwave is from the brand " + this.brand);
	}
	public void reheat(int seconds)
	{
		for (int i = seconds;i >= 0; i--)
		{
			System.out.println("Item will be heated in " + i);
		}
	}
}